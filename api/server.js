const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config");

const users = require('./app/users');
const car = require('./app/car');
const carHistory = require('./app/carHistory');

const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

db.once("open", () => {
    console.log("Mongoose connected!");

    app.use('/users', users());
    app.use('/cars', car());
    app.use('/history', carHistory());

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});