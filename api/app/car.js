const express = require("express");
const Car = require("../models/Car");
const multer = require("multer");
const nanoid = require("nanoid");
const path = require("path");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const config = require("../config");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cd) => {
    cd(null, config.uploadPath);
  },
  filename: (req, file, cd) => {
    cd(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

/*Роут для пользователей, видят только опубликованные*/
const createRouter = () => {
  router.get("/", (req, res) => {
    Car.find({ published: true })
      .then(car => res.send(car))
      .catch(() => res.sendStatus(500));
  });



  /*Роут для админа, видят все объявления*/
  router.get("/admin", [auth, permit("admin")], (req, res) => {
    Car.find()
      .then(car => {
        if (car) res.send(car);
        else res.sendStatus(404);
      })
      .catch((err) => {
        res.status(500).send(err)
      });
  });

  router.get("/:id", (req, res) => {
    const id = req.params.id;
    Car.findById(id)
    .then(result => {
      if (result) res.send(result);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
  });

  router.post("/", [auth, upload.single("image")], async (req, res) => {
    const car = new Car(req.body);

    car.author = req.user._id;

    if (req.file) {
      car.image = req.file.filename;
    } else {
      car.image = null;
    }

    try {
      await car.save();
      res.send({message: car.brand + ' was added to auction'});
    } catch (e) {
      res.status(400).send({message: "Can not add car to auction"});
    }

  });

  /*Одобрение админом*/
  router.post("/approve", [auth, permit("admin")], async (req, res) => {
    const item = await Car.findOne({ _id: req.body.id });

    if (!item) {
      return res.status(400).send({ error: "Car not found" });
    }

    item.published = true;
    item.publishedTime = Date.now()+1000*60*10;
    item
      .save()
      .then(car => res.send(car))
      .catch(error => res.status(400).send(error));
  });

  /*Удаление админом*/
  router.delete("/:id", [auth, permit("admin")], (req, res) => {
    console.log(req.params.id);
    Car.deleteOne({ _id: req.params.id })
      .then(response => res.send("Success"))
      .catch(err => res.send(err));
  });

  return router;
};

module.exports = createRouter;
