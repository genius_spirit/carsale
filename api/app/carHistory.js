const express = require('express');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const CarHistory = require('../models/CarHistory');
const Car = require('../models/Car');

const router = express.Router();

const createRouter = () => {
    // Получение всей истории ставок на определенный автомобиль.

    router.get('/:id', async(req, res) => {
      const carId = req.params.id;
      try {
        const history = await CarHistory
        .find({car: carId})
        .populate("buyer")
        .sort({date: -1});
        if (history) res.send(history);
      } catch (e) {
        return res.status(500).send({message: "Bad request. History not found!"})
      }
    });

    /* Покупателя находим по токену, дата генерируется автоматом, для того что бы видеть хронологию ставок
     amount должен быть выше текущего
     */

    router.post('/', [auth, permit('user', 'admin')], async (req, res) => {

        const car = await Car.findOne({_id: req.body.car});
        if (car) {
          if (req.body.amount <= car.price) {
            return res.status(400).send({ message: 'Your bet less then price of car' });
          }
        } else {
          return res.status(404).send({message: 'Car not found'});
        }
        
        const history = await CarHistory.find({car: req.body.car});
        if (history) {
          history.map(item => {
            if (item.amount >= req.body.amount)
              return res.sendStatus(400).send({message: 'Your bet less then price of car'});
          })
        }

        const hist = new CarHistory({
            amount: req.body.amount,
            buyer: req.user._id,
            car: req.body.car
        });

        try {
          await hist.save();
          return res.status(200).send({message: 'Bet successful'});
        } catch (e) {
          return res.status(500).send({message: 'Send wrong data'})
        }

    });

    return router;
};

module.exports = createRouter;