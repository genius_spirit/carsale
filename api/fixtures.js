const mongoose = require("mongoose");
const config = require("./config");

const User = require("./models/User");
const Car = require("./models/Car");
const CarBody = require("./models/CarBody");

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

const collections = ["cars", "users"];

db.once("open", async () => {
  collections.forEach(async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });

  const [ivan, igor, admin] = await User.create(
    {
      username: "ivan",
      password: "ivan123",
      phone: 74441234567,
      role: "user"
    },
    {
      username: "igor",
      password: "igor123",
      phone: 47774567890,
      role: "user"
    },
    {
      username: "admin",
      password: "admin123",
      phone: 11112223344,
      role: "admin"
    }
  );

  const [sedan, suv, minivan, wagon, hatchback, coupe] = await CarBody.create(
    {
      body: "Sedan"
    },
    {
      body: "SUV"
    },
    {
      body: "Minivan"
    },
    {
      body: "Wagon"
    },
    {
      body: "Hatchback"
    },
    {
      body: "Coupe"
    }
  );

  await Car.create(
    {
      brand: "Mercedes",
      model: "E220 (W212)",
      engine: 2.2,
      year: 2012,
      color: "black",
      millage: "54287",
      price: "12499",
      image: "mercedesE220.jpeg",
      about: "The best of the best",
      body: sedan.body,
      author: ivan._id,
      published: true,
      publishedTime: Date.now()+1000*60*10
    },
    {
      brand: "Toyota",
      model: "Camry",
      engine: 3.5,
      year: 2012,
      color: "black",
      millage: "100123  ",
      price: "11699",
      image: "camry.jpg",
      author: igor._id,
      about: "The best of the best",
      body: sedan.body,
      published: true,
      publishedTime: Date.now()+1000*60*10
    },
    {
      brand: "AUDI",
      model: "Q7 TDI quattro ",
      engine: 3.0,
      year: 2011,
      color: "White",
      millage: "149062",
      price: "19699",
      image: "audiQ7.jpeg",
      about: "The best of the best",
      body: suv.body,
      author: ivan._id,
      published: true,
      publishedTime: Date.now()+1000*60*10
    },
    {
      brand: "BMW M SERIES",
      model: "X6 M",
      engine: 4.4,
      year: 2015,
      color: "Grey",
      millage: "18689",
      price: "78000",
      image: "bmwX6.jpeg",
      author: igor._id,
      about: "The best of the best",
      body: suv.body,
      published: false
    },
    {
      brand: "MERCEDES-BENZ",
      model: "V-Class AVANTGARDE Long",
      engine: 2.1,
      year: 2016,
      color: "Black",
      millage: "171",
      price: "75000",
      image: "mersVclass.jpeg",
      about: "The best of the best",
      body: minivan.body,
      author: ivan._id,
      published: true,
      publishedTime: Date.now()+1000*60*10
    },
    {
      brand: "MERCEDES-BENZ",
      model: "CLS 500 4MATIC BE",
      engine: 4.7,
      year: 2013,
      color: "Silver",
      millage: "39516",
      price: "28500",
      image: "mersCLS.jpeg",
      about: "The best of the best",
      body: wagon.body,
      author: ivan._id,
      published: false
    },
    {
      brand: "INFINITI EX",
      model: "2.5 V6",
      engine: 2.5,
      year: 2012,
      color: "White",
      millage: "95726",
      price: "16450",
      image: "infinity.jpeg",
      author: igor._id,
      about: "The best of the best",
      body: wagon.body,
      published: true,
      publishedTime: Date.now()+1000*60*10

    },
    {
      brand: "ROLLS-ROYCE PHANTOM",
      model: "RR3 Phantom Coupe",
      engine: 6.7,
      year: 2015,
      color: "Silver",
      millage: "6170",
      price: "270499",
      image: "rolls.jpeg",
      about: "The best of the best",
      body: coupe.body,
      author: ivan._id,
      published: true,
      publishedTime: Date.now()+1000*60*10
    },
    {
      brand: "PORSCHE CAYMAN",
      model: "3.4 Cayman S",
      engine: 3.4,
      year: 2006,
      color: "Red",
      millage: "94822",
      price: "16999",
      image: "porsche.jpeg",
      author: igor._id,
      about: "The best of the best",
      body: coupe.body,
      published: true,
      publishedTime: Date.now()+1000*60*10
    },
    {
      brand: "Nissan",
      model: "Teana",
      engine: 2.5,
      year: 2009,
      color: "black",
      millage: "115375  ",
      price: "8200",
      image: "nissan_teana.jpeg",
      author: igor._id,
      about: "The best of the best",
      body: sedan.body,
      published: true,
      publishedTime: Date.now()+1000*60*10
    },
    {
      brand: "Volvo",
      model: "S40",
      engine: 2.0,
      year: 2011,
      color: "white",
      millage: "103049  ",
      price: "9000",
      image: "volvoS40.jpeg",
      author: igor._id,
      about: "The best of the best",
      body: sedan.body,
      published: true,
      publishedTime: Date.now()+1000*60*10
    }
  );

  db.close();
});
