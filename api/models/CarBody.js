const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CarBodySchema = new Schema({
  body: {
    type: String,
    enum: ['Sedan', 'SUV', 'Minivan', 'Wagon', 'Hatchback', 'Coupe'],
    required: true
  }
});

const CarBody = mongoose.model('CarBody', CarBodySchema);

module.exports = CarBody;