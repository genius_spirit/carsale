const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CarHistorySchema = new Schema({
    car: {
        type: Schema.Types.ObjectId,
        ref: 'Car',
        required: true
    },
     buyer: {
         type: Schema.Types.ObjectId,
         ref: 'User',
         required: true
    },
    /*начальная стоимость*/
    amount: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

const CarHistory = mongoose.model('CarHistory', CarHistorySchema);

module.exports = CarHistory;