const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CarSchema = new Schema({
    brand: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    engine: {
      type: Number,
      required: true,
      min: 0.5,
      max: 7
    },
    year: {
        type: Number,
        required: true
    },
    color: {
      type: String,
      required: true
    },
    millage: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        default: 0,
      required: true
    },
    image: {
      type: String,
      required: true
    },
    about: String,
    body: {
      type: String,
      ref: 'CarBody',
      required: true
    },
    /*автор объявления*/
    author:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    /*флаг одобрения админом*/
    published: {
        type: Boolean,
        default: false
    },
    /*дата публикации, задается автоматически в момент одобрения админом*/
    publishedTime: {
        type: Number,
        default: 0
    }
});

const Car = mongoose.model('Car', CarSchema);

module.exports = Car;