import React, {Fragment} from "react";
import {
  Button,
  Card,
  CardBody, CardFooter,
  CardImg,
  CardSubtitle,
  CardText,
  CardTitle, Col
} from "reactstrap";
import PropTypes from "prop-types";
import config from "../../config";

import notFound from "../../assets/images/not-found.png";

const ListItem = props => {
  let image = notFound;

  if (props.image) {
    image = config.apiUrl + "uploads/" + props.image;
  }

  return (
    <Fragment>
      <Col sm="4">
      <Card style={{marginBottom: '20px'}}>
        {props.image !== "hide" ? (
          <CardImg
            top
            width="100%"
            src={image}
            alt="Artist Image"
            style={{ width: "95%", marginRight: "55%" }}
          />
        ) : null}
        <CardBody style={{paddingBottom: 25}}>
          <CardTitle tag="h3">
            {props.name}
          </CardTitle>
          <CardSubtitle tag="h4">{props.model}</CardSubtitle>
          <CardText>{"Год выпуска:  " + props.year + " г"}</CardText>
          <CardText>{"Пробег: " + props.millage + " км"}</CardText>
          <CardText>{"Объем двигателя: " + props.engine + " л"}</CardText>
          <CardText><strong>{"Цена: " + props.price}</strong></CardText>
          <CardFooter>
            {!props.public ? (
              props.buttonName ? (
                <Button color="success" onClick={props.click}>
                  {props.buttonName}
                </Button>
              ) : null
            ) : null}
          {props.optionalField ? (
            <CardSubtitle>{props.optionalField}</CardSubtitle>
          ) : null}
          {props.delete ? (
            <Button onClick={props.delete} color="danger">
              delete
            </Button>
          ) : null}
            {props.show ? (
              <Button onClick={props.show} color="info">
                show
              </Button>
            ) : null}
          </CardFooter>
        </CardBody>
      </Card>
      </Col>
    </Fragment>

  );
};

ListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string,
  click: PropTypes.func,
  buttonName: PropTypes.string,
  num: PropTypes.number,
  optionalField: PropTypes.string,
  delete: PropTypes.func
};

export default ListItem;
