import React from "react";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { NavbarBrand } from "reactstrap";
import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";

const Toolbar = ({ user, logout }) => {
  return (
    <Navbar style={{fontSize: '20px'}}>
      <NavbarBrand href="/">React Trade</NavbarBrand>
      <Navbar.Header>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav >
          <LinkContainer to="/sell">
            <NavItem>Sell</NavItem>
          </LinkContainer>
          <LinkContainer to="/auction">
            <NavItem>Buy</NavItem>
          </LinkContainer>
        </Nav>
        <Nav pullRight>
          {user ? (
            <UserMenu logout={logout} user={user.user.username} />
          ) : (
            <AnonymousMenu />
          )}
          {user && user.user.role === "admin" ? (
            <LinkContainer to="/admin" exact>
              <NavItem>Admin Panel</NavItem>
            </LinkContainer>
          ) : null}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Toolbar;
