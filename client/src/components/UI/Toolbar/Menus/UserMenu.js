import React from 'react';

import {LinkContainer} from "react-router-bootstrap";
import { Nav, NavItem } from "react-bootstrap";

const UserMenu=({user,logout})=>(
    <Nav pullRight>
        <NavItem>{'Hello, ' + user}</NavItem>
        <LinkContainer to="/logout" exact>
            <NavItem onClick={logout}>Logout</NavItem>
        </LinkContainer>
        {/*<LinkContainer to="/add" exact>*/}
            {/*<NavItem>Add Car</NavItem>*/}
        {/*</LinkContainer>*/}

    </Nav>
);

export default UserMenu;