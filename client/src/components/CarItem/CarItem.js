import React from "react";
import { Card, CardBody, CardFooter, CardImg, CardText, CardTitle } from "reactstrap";
import { Col, PageHeader, Panel, Row } from "react-bootstrap";

const CarItem = ({brand, model, year, engine, millage, body, color, about, image}) => {
  return (
    <Card>
      <PageHeader>{`${brand} ${model}`}</PageHeader>
      <Row>
        <Col md={6}>
          <Panel>
            <CardImg width="100%" src={image}/>
          </Panel>
        </Col>

        <Col md={6}>
          <CardBody>
            <CardTitle>Year: <strong>{year}</strong></CardTitle>
            <CardTitle>Engine: <strong>{engine}</strong></CardTitle>
            <CardTitle>Millage: <strong>{millage}km</strong></CardTitle>
            <CardTitle>Body: <strong>{body}</strong></CardTitle>
            <CardTitle>Color: <strong>{color}</strong></CardTitle>
            <CardText>{about}</CardText>
          </CardBody>
        </Col>
      </Row>
      <CardFooter>Options</CardFooter>
    </Card>
  );
};

export default CarItem;
