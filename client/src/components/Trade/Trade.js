import React, { Component, Fragment } from "react";
import moment from 'moment';
import { Col, PageHeader, Panel, Row, Table } from "react-bootstrap";
import AddBetForm from "../../containers/AddBetForm/AddBetForm";

class Trade extends Component {
  state = {
    time: 0
  };

  componentDidMount() {
    this.interval = setInterval(this.setTime, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentDidUpdate() {
    if (this.props.time - Date.now() < 0) {
      clearInterval(this.interval);
    }
  }

  setTime = () => {
    let timer;
    this.props.time - Date.now() > 0
      ? (timer = this.props.time - Date.now())
      : (timer = 0);
    console.log(timer, "Timer", this.props.time - Date.now());
    this.setState({
      time: timer
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader>
          Auction{" "}
          {!this.props.user && <small>You need to login for buy car</small>}
        </PageHeader>
        <Row>
          <Col md={12}>
            <Panel bsStyle="primary">
              <Panel.Heading style={{ textAlign: "center", fontSize: "20px" }}>
                <Panel.Title>
                  {"First price: " + this.props.price + " $"}
                </Panel.Title>
                Until the end of trading: {moment(this.state.time).format('h:mm:ss')}
              </Panel.Heading>
              <Panel.Body>
                {this.state.time > 0 ? (
                  <AddBetForm carId={this.props.carId} />
                ) : null}
                <Col smOffset={1} sm={10}>
                  <PageHeader>Best bets</PageHeader>
                  <Table striped condensed hover responsive>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Bet</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.props.history &&
                        this.props.history.map((item, index) => (
                          <tr key={item._id}>
                            <td>{index + 1}</td>
                            <td>{item.buyer.username}</td>
                            <td>{item.amount + "$"}</td>
                            <td>{item.date}</td>
                          </tr>
                        ))}
                    </tbody>
                  </Table>
                </Col>
              </Panel.Body>
            </Panel>
          </Col>
        </Row>
      </Fragment>
    );
  }
}

export default Trade;
