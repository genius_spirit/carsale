import React, {Component} from "react";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import Auction from "./containers/Auction/Auction";
import AddCar from "./containers/AddCar/AddCar";
import Admin from "./containers/Admin/Admin";
import AboutCar from "./containers/AboutCar/AboutCar";
import Main from "./containers/Main/Main";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Main}/>
                    <Route path="/auction" exact component={Auction}/>
                    <Route path="/about-car/:id" exact component={AboutCar}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/sell" exact component={AddCar}/>
                    <Route path="/admin" exact component={Admin}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
