import React, { Component } from "react";
import { connect } from "react-redux";
import ListItem from "../../components/ListItem/ListItem";
import { CardColumns } from "reactstrap";
import { getCars } from "../../store/actions/cars";

class Auction extends Component {
  componentDidMount() {
    this.props.getCars();
  }

  render() {
    return (
      <CardColumns>
        {this.props.cars &&
          Array.isArray(this.props.cars) &&
          this.props.cars.map(car => {
            return (
              <ListItem
                key={car._id}
                millage={car.millage}
                engine={car.engine}
                name={car.brand}
                model={car.model}
                year={car.year}
                image={car.image}
                price={car.price}
                id={car._id}
                click={() => this.props.history.push("/about-car/" + car._id)}
                buttonName="About car"
              />
            );
          })}
      </CardColumns>
    );
  }
}
const mapStateToProps = state => ({
  cars: state.cars.cars,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getCars: () => dispatch(getCars())
});

export default connect(mapStateToProps, mapDispatchToProps)(Auction);
