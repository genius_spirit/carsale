import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { getCars } from "../../store/actions/cars";
import Carousel from "react-bootstrap/es/Carousel";
import config from "../../config";

class Main extends Component {
  componentDidMount() {
    this.props.getCars();
  }

  render() {
    return (
      <Fragment>
        <h1 align="center">Онлайн-автоаукцион автомобилей с пробегом</h1>
        <Carousel indicators={false} controls={false} pauseOnHover={true}>
          {Array.isArray(this.props.cars)
            ? this.props.cars.map(item => {
                if (item.image) {
                  return (
                    <Carousel.Item
                      key={item._id}
                      onClick={() =>
                        this.props.history.push("/about-car/" + item._id)
                      }
                    >
                      <img
                        style={{ margin: "20px auto 0" }}
                        width={700}
                        alt="900x600"
                        src={config.apiUrl + "uploads/" + item.image}
                      />
                      <Carousel.Caption>
                        <h3>{item.brand + " " + item.model}</h3>
                        <h4>{item.price + "$"}</h4>
                      </Carousel.Caption>
                    </Carousel.Item>
                  );
                } else return null;
              })
            : null}
        </Carousel>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cars: state.cars.cars
});

const mapDispatchToProps = dispatch => ({
  getCars: () => dispatch(getCars())
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
