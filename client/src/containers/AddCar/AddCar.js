import React, { Component, Fragment } from "react";

import {
  Button,
  Col,
  Form,
  FormGroup,
  PageHeader
} from "react-bootstrap";
import { connect } from "react-redux";
import Alert from "react-bootstrap/es/Alert";
import FormElement from "../../components/UI/Form/FormElement";
import { postCar } from "../../store/actions/cars";

class Login extends Component {
  state = {
    brand: "",
    model: "",
    engine: 0,
    year: 0,
    color: "",
    millage: "",
    price: "",
    image: "",
    about: "",
    body: ""
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });
    this.props.postCar(formData);
  };

  render() {
    const bodies = [
      { title: "Sedan" },
      { title: "SUV" },
      { title: "Minivan" },
      { title: "Wagon" },
      { title: "Hatchback" },
      { title: "Coupe" }
    ];

    return (
      <Fragment>
        <PageHeader>Add car for sale {!this.props.user && <small>You need to login for sell car</small>}</PageHeader>
        <Form horizontal onSubmit={this.submitFormHandler}>
          {this.props.error && (
            <Alert bsStyle="danger">{this.props.error.errors}</Alert>
          )}

          <FormElement
            propertyName="brand"
            title="Brand"
            placeholder="Enter Brand"
            type="text"
            value={this.state.brand}
            changeHandler={this.inputChangeHandler}
            autoComplete="current-brand"
            required
          />

          <FormElement
            propertyName="model"
            title="Model"
            placeholder="Enter model"
            type="text"
            value={this.state.model}
            changeHandler={this.inputChangeHandler}
            autoComplete="current-model"
            required
          />
          <FormElement
            propertyName="body"
            title="Body of car"
            type="select"
            options={bodies}
            value={this.state.body}
            changeHandler={this.inputChangeHandler}
            required
          />
          <FormElement
            propertyName="engine"
            title="Engine"
            placeholder="Enter engine"
            type="text"
            value={this.state.engine}
            changeHandler={this.inputChangeHandler}
            autoComplete="current-engine"
            required
          />
          <FormElement
            propertyName="year"
            title="Year"
            placeholder="Enter year"
            type="number"
            value={this.state.year}
            changeHandler={this.inputChangeHandler}
            autoComplete="current-year"
            required
          />
          <FormElement
            propertyName="color"
            title="Color"
            placeholder="Enter color"
            type="text"
            value={this.state.color}
            changeHandler={this.inputChangeHandler}
            autoComplete="current-color"
            required
          />
          <FormElement
            propertyName="millage"
            title="Millage"
            placeholder="Enter millage"
            type="number"
            value={this.state.millage}
            changeHandler={this.inputChangeHandler}
            autoComplete="current-millage"
            required
          />
          <FormElement
            propertyName="price"
            title="Price"
            placeholder="Enter price"
            type="number"
            value={this.state.price}
            changeHandler={this.inputChangeHandler}
            autoComplete="current-price"
            required
          />
          <FormElement
            propertyName="about"
            title="About"
            placeholder="Enter about"
            type="textarea"
            value={this.state.about}
            changeHandler={this.inputChangeHandler}
            autoComplete="current-about"
          />
          <FormElement
            propertyName="image"
            title="Image of car"
            type="file"
            changeHandler={this.fileChangeHandler}
            required
          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary"
                      type="submit"
                      style={{marginTop: '10px'}}
                      disabled={!this.props.user} >
                Add car
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  postCar: data => dispatch(postCar(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
