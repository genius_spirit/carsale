import React, { Component } from "react";
import { connect } from "react-redux";
import { Alert, Button, Col, Form, FormGroup } from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import { takeBetOnCar } from "../../store/actions/auction";

class AddBetForm extends Component {

  state = {
    bet: 0
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();
    const data = {car: this.props.carId,  amount: this.state.bet};
    this.props.takeBetOnCar(data, this.props.carId);
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        {this.props.error && (
          <Alert bsStyle="danger">{this.props.error.errors}</Alert>
        )}

        <FormElement
          propertyName="bet"
          title="Your bet"
          placeholder="your bet"
          type="number"
          value={this.state.bet}
          changeHandler={this.inputChangeHandler}
          autoComplete="current-bet"
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit" disabled={!this.props.user}>Add bet</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  takeBetOnCar: (data, id) => dispatch(takeBetOnCar(data, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddBetForm);