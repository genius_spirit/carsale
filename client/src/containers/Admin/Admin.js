import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import ListItem from "../../components/ListItem/ListItem";
import {approveCar, deleteCar, getAllCars} from "../../store/actions/cars";

class Admin extends Component {

    componentDidMount() {
        this.props.getAllCars();
    }

    render() {
        return (
            <Fragment>
                {this.props.cars ? (
                    this.props.cars.map(car => {
                        return (
                            <ListItem
                                year={car.year}
                                millage={car.millage}
                                engine={car.engine}
                                price={car.price}
                                key={car._id}
                                name={car.brand}
                                image={car.image}
                                id={car._id}
                                click={() => this.props.approveCar(car._id)}
                                buttonName="approve"
                                delete={() => this.props.deleteCar(car._id)}
                                public={car.published}
                                show={() => this.props.history.push('/about-car/' + car._id)}
                            />
                        );
                    })
                ) : (
                    <p style={{padding: "30px", fontSize: "25px"}}>Пусто</p>
                )}
            </Fragment>
        );
    }
}
const mapStateToProps = state => ({
    cars: state.cars.allCars
});

const mapDispatchToProps = dispatch => ({
    getAllCars: () => dispatch(getAllCars()),
    deleteCar: data => dispatch(deleteCar(data)),
    approveCar: data => dispatch(approveCar(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(Admin);
