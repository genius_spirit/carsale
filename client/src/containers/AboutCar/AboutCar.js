import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import config from "../../config";
import CarItem from "../../components/CarItem/CarItem";
import Trade from "../../components/Trade/Trade";
import {getCarById} from "../../store/actions/cars";
import { getBetsHistoryOfCar } from "../../store/actions/auction";

class AboutCar extends Component {

    componentDidMount() {
        this.props.getCarById(this.props.match.params.id);
        this.props.getBetsHistoryOfCar(this.props.match.params.id)
    }

    render() {

        let image;
        if (this.props.cars.image) {
            image = config.apiUrl + "uploads/" + this.props.cars.image;
        } else return null;

        return (
            this.props.cars ?
                <Fragment>
                    <CarItem
                        brand={this.props.cars.brand}
                        model={this.props.cars.model}
                        image={image}
                        year={this.props.cars.year}
                        engine={this.props.cars.engine}
                        millage={this.props.cars.millage}
                        body={this.props.cars.body}
                        color={this.props.cars.color}
                        about={this.props.cars.about}
                    />
                    <Trade
                      carId={this.props.cars._id}
                      price={this.props.cars.price}
                      history={this.props.history}
                      user={this.props.user}
                      time={this.props.cars.publishedTime}
                    />
                </Fragment>
                : null
        )
    }
}

const mapStateToProps = state => {
    return {
        cars: state.cars.cars,
        history: state.auction.history,
        user: state.users.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getCarById: id => dispatch(getCarById(id)),
        getBetsHistoryOfCar: id => dispatch(getBetsHistoryOfCar(id))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(AboutCar);
