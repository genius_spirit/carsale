import axios from "../../axios-api";
import {push} from "react-router-redux";
import { NotificationManager } from "react-notifications";
import {
  GET_ALL_CARS_FAILURE,
  GET_ALL_CARS_SUCCESS,
  GET_CAR_BY_ID_FAILURE,
  GET_CAR_BY_ID_SUCCESS,
  GET_CARS_FAILURE,
  GET_CARS_SUCCESS, POST_NEW_CAR_FAILURE, POST_NEW_CAR_SUCCESS
} from "./actionTypes";

const getCarsSuccess = cars => {
    return {type: GET_CARS_SUCCESS, cars};
};

const getCarsFailure = error => {
    return {type: GET_CARS_FAILURE, error};
};

export const getCars = () => {
    return dispatch => {
        axios.get("/cars").then(
            response => {
                dispatch(getCarsSuccess(response.data));
            },
            error => {
                dispatch(getCarsFailure(error));
            }
        );
    };
};

const getCarByIdSuccess = data => {
    return {type: GET_CAR_BY_ID_SUCCESS, data};
};

const getCarByIdFailure = error => {
    return {type: GET_CAR_BY_ID_FAILURE, error};
};

export const getCarById = id => dispatch => {
    axios.get(`/cars/${id}`).then(
        response => dispatch(getCarByIdSuccess(response.data)),
        error => dispatch(getCarByIdFailure(error))
    )
};

const getAllCarsSuccess = allCars => {
    return {type: GET_ALL_CARS_SUCCESS, allCars};
};

const getAllCarsFailure = error => {
  return {type: GET_ALL_CARS_FAILURE, error};
};

export const getAllCars = () => {
    return dispatch => {
        axios.get("/cars/admin").then(
            response => dispatch(getAllCarsSuccess(response.data)),
            error => dispatch(getAllCarsFailure(error))
        );
    };
};

const postCarFailure = error => {
  return {type: POST_NEW_CAR_FAILURE, error};
};

export const postCar = data => {
    return dispatch => {
        return axios.post("/cars", data).then(
            response => {
                dispatch({type: POST_NEW_CAR_SUCCESS});
                NotificationManager.success(`${response.data.message}`, "Success", 5000);
                dispatch(push("/"));
            },
            error => dispatch(postCarFailure(error))
        );
    };
};

export const approveCar = data => {
    return (dispatch, getState) => {
        const token = getState().users.user.user.token;
        const headers = {Token: token};
        return axios.post("/cars/approve", {id: data}, {headers}).then(
            response => {
                dispatch(getAllCars());
            },
            error => console.log(error)
        );
    };
};

export const deleteCar = data => {
    return (dispatch, getState) => {
        const token = getState().users.user.user.token;
        const headers = {Token: token};

        return axios.delete("/cars/" + data, {headers}).then(
            response => {
                dispatch(getAllCars());
            },
            error => {
                console.log(error);
            }
        );
    };
};

