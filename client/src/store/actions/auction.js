import axios from "../../axios-api";
import { NotificationManager } from "react-notifications";
import {
  GET_BETS_HISTORY_OF_CAR_FAILURE,
  GET_BETS_HISTORY_OF_CAR_SUCCESS,
  TAKE_BET_ON_CAR_FAILURE
} from "./actionTypes";


const takeBetOnCarFailure = error => {
  return {type: TAKE_BET_ON_CAR_FAILURE, error};
};

export const takeBetOnCar = (data, id) => {
  return dispatch => {
    return axios.post("/history", data).then(
      response => {
        dispatch(getBetsHistoryOfCar(id));
        NotificationManager.success(`${response.data.message}`, "Success", 5000);
      },
      error => {
        dispatch(takeBetOnCarFailure(error));
        NotificationManager.error(`${error.response.data.message}`, "Error", 5000);
      }
    )
  }
};

const getBetsHistoryOfCarSuccess = data => {
  return {type: GET_BETS_HISTORY_OF_CAR_SUCCESS, data};
};

const getBetsHistoryOfCarFailure = error => {
  return {type: GET_BETS_HISTORY_OF_CAR_FAILURE, error};
};

export const getBetsHistoryOfCar = id => {
  return dispatch => {
    axios.get(`/history/${id}`).then(
      response => {
        dispatch(getBetsHistoryOfCarSuccess(response.data));
      },
      error => {
        dispatch(getBetsHistoryOfCarFailure(error.response))
      }
    )
  }
};