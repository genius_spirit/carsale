import { routerMiddleware, routerReducer } from "react-router-redux";
import thunkMiddleware from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";

import { readState, saveState } from "./localStorage";

import usersReducer from "./reducers/user";
import carsReducer from "./reducers/cars";
import auctionReducer from "./reducers/auction";


const rootReducer = combineReducers({
    users: usersReducer,
    cars: carsReducer,
    auction: auctionReducer,
    routing: routerReducer
});

export const history = createHistory();

const middleware = [
  thunkMiddleware,
  routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(...middleware));

const persistedState = readState();

const store = createStore(rootReducer, persistedState, enhancer);

store.subscribe(() => {
  saveState({users: {user: store.getState().users.user}});
});

export default store;