import {
  GET_BETS_HISTORY_OF_CAR_FAILURE,
  GET_BETS_HISTORY_OF_CAR_SUCCESS,
  TAKE_BET_ON_CAR_FAILURE
} from "../actions/actionTypes";

const initialState = {
  history: [],
  betError: '',
  historyError: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TAKE_BET_ON_CAR_FAILURE:
      return {...state, betError: action.error};
    case GET_BETS_HISTORY_OF_CAR_SUCCESS:
      return {...state, history: action.data};
    case GET_BETS_HISTORY_OF_CAR_FAILURE:
      return {...state, historyError: action.error};
    default:
      return state;
  }
};

export default reducer;