import {
  GET_ALL_CARS_SUCCESS,
  GET_CAR_BY_ID_FAILURE,
  GET_CAR_BY_ID_SUCCESS,
  GET_CARS_FAILURE,
  GET_CARS_SUCCESS
} from "../actions/actionTypes";
const initialState = {
  cars: [],
  error: false,
  loaded: false,
  allCars: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CARS_SUCCESS:
      return { ...state, cars: action.cars };
    case GET_CARS_FAILURE:
      return { ...state, error: action.error };
    case GET_ALL_CARS_SUCCESS:
      return { ...state, allCars: action.allCars };
    case GET_CAR_BY_ID_SUCCESS:
      return { ...state, cars: action.data };
    case GET_CAR_BY_ID_FAILURE:
      return { ...state, error: action.error };
    default:
      return state;
  }
};

export default reducer;
