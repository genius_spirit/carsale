import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store, {history} from "./store/configureStore";
import {ConnectedRouter} from 'react-router-redux';
import axios from './axios-api';
import 'react-notifications/lib/notifications.css';

import './index.css';
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { logoutExpiredUser } from "./store/actions/user";

axios.interceptors.request.use(config => {
  try {
    config.headers['Token'] = store.getState().users.user.token;
  } catch (e) {
    //nothing
  }
  return config;
});

axios.interceptors.response.use(response => response, error => {
  if (error.response.status === 401) {
    store.dispatch(logoutExpiredUser());
  } else return Promise.reject(error);
});

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
